The API runs on port 3000 by default.

You can start the api by running `yarn dev`, after running `yarn install`, of course.

You can run the jest test suite by running `yarn test`.

Your instructions say there should be 87 people, but Swapi only has 82.

The tests sometimes fail because they take too long to run. If I had more time I'd write them differently.

The Swapi API has also been giving me random 404's sporadically, and sometimes taking far too long to respond. I think it's probably falling back to a 404 instead of a 500 for whatever reason. Hopefully you'll run it at a good time without issues.

Ideas for future improvement:

1. Caching
2. Better error handling
3. Retry system for when Swapi fails

Thanks! This was fun.
