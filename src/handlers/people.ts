import { Request, Response } from 'express';

import SwapiAPI from '../external-apis/swapi';
import { People } from '../interfaces';

export default async function getPeople(req: Request, res: Response): Promise<Response<People[]>> {
  const { sortBy } = req.query;

  try {
    const rawPeople = await SwapiAPI.getPeople();
    const sortedPeople = sortPeople(rawPeople, sortBy as SortBy);

    return res.status(200).send(sortedPeople);
  } catch (e) {
    return res.status(500).send(new Error(
      'Error - Swapi is likely not responding'
    ));
  }
}

enum SortBy {
  Height = 'height',
  Mass = 'mass',
  Name = 'name'
}

function sortPeople(people: People[], sortBy: SortBy): People[] {
  if (sortBy === SortBy.Name) {
    return people.sort((a, b) => {
      const nameA = a.name.toUpperCase();
      const nameB = b.name.toUpperCase();
      return (nameA < nameB) ? -1 : (nameA > nameB) ? 1 : 0;
    });
  } if (sortBy === SortBy.Height) {
    return people.sort((a, b) => {
      let heightA = parseFloat(a.height.replace(/,/g, ''));
      let heightB = parseFloat(b.height.replace(/,/g, ''));
      if (a.height === 'unknown') heightA = 0;
      if (b.height === 'unknown') heightB = 0;

      return (heightA < heightB) ? -1 : (heightA > heightB) ? 1 : 0;
    });
  } if (sortBy === SortBy.Mass) {
    return people.sort((a, b) => {
      let massA = parseFloat(a.mass.replace(/,/g, ''));
      let massB = parseFloat(b.mass.replace(/,/g, ''));
      if (a.mass === 'unknown') massA = 0;
      if (b.mass === 'unknown') massB = 0;

      return (massA < massB) ? -1 : (massA > massB) ? 1 : 0;
    });
  }
  return people;
}
