/* eslint-disable @typescript-eslint/no-use-before-define */
/* eslint-disable import/no-extraneous-dependencies */
import request from 'supertest';

import app from '../app';

describe('PlanetHandler#getPlanets', () => {
  let response: any;
  beforeAll(async () => {
    response = await request(app)
      .get('/planets');
  });

  it('should return a 200', () => { expect(response.statusCode).toBe(200); });
  it('should return the correct amount of data', () => {
    expect(response.body.length).toBe(60);
  });
  it('should replace resident urls with names', () => {
    expect(response.body[0].residents).toStrictEqual([
      'Luke Skywalker',
      'C-3PO',
      'Darth Vader',
      'Owen Lars',
      'Beru Whitesun lars',
      'R5-D4',
      'Biggs Darklighter',
      'Anakin Skywalker',
      'Shmi Skywalker',
      'Cliegg Lars']);
  });

  afterAll((done) => {
    done();
  });
});
