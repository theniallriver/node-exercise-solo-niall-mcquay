import { Request, Response } from 'express';

import SwapiAPI from '../external-apis/swapi';
import { Planet } from '../interfaces';

export default async function getPlanets(req: Request, res: Response): Promise<Response<Planet[]>> {
  try {
    const [rawPlanets, rawPeople] = await Promise.all([SwapiAPI.getPlanets(), SwapiAPI.getPeople()]);

    const planetsWithResidents = rawPlanets.map((planet) => {
      const residentNames = planet.residents.map((residentUrl) => rawPeople.find((p) => p.url === residentUrl)?.name);
      return {
        ...planet,
        residents: residentNames
      };
    });

    return res.status(200).send(planetsWithResidents);
  } catch (e) {
    return res.status(500).send('Error - Swapi may be having issues');
  }
}
