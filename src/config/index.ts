interface Config {
  PORT: number
  SWAPI_API_URL: string

}

const config: Config = {
  PORT: 3000,
  SWAPI_API_URL: 'https://swapi.dev/api'
};

export default config;
