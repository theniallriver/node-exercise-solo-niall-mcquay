import SwapiAPI from './index';

describe('SwapiApi', () => {
  describe('#getPlanets', () => {
    test('it gets all 60 planets from Swapi', () => SwapiAPI.getPlanets().then((planets) => {
      expect(planets.length).toBe(60);
    }));
  });

  describe('#getPeople', () => {
    test('it gets all 82 people from Swapi', () => SwapiAPI.getPeople().then((people) => {
      expect(people.length).toBe(82);
    }));
  });
});
