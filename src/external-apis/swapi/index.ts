import axios from 'axios';

import config from '../../config';
import { People, Planet } from '../../interfaces';

const { SWAPI_API_URL } = config;

const SwapiAPI = {
  async getPeople(): Promise<People[]> {
    try {
      return await getPaginatedSwapiResponse(`${SWAPI_API_URL}/people`, 9);
    } catch (e) {
      console.error(e);
      throw e;
    }
  },
  async getPlanets(): Promise<Planet[]> {
    try {
      return await getPaginatedSwapiResponse(`${SWAPI_API_URL}/planets`, 6);
    } catch (e) {
      console.error(e);
      throw e;
    }
  }
};

export default SwapiAPI;

async function getPaginatedSwapiResponse(baseUrl: string, pageCount: number) {
  try {
    const requestUrls = new Array(pageCount).fill('').map((_u, i) => `${baseUrl}?page=${i + 1}`);
    const [...responses] = await Promise.all(requestUrls.map((url) => axios.get(url)));
    return responses.map((x) => x.data.results).flat();
  } catch (e) {
    console.error(e);
    throw e;
  }
}
