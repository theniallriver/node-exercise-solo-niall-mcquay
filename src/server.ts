import app from './app';
import config from './config';

app.listen(config.PORT, () => {
  console.log(`Api running on http://localhost:${config.PORT}`);
});
