enum Gender {
  male = 'Male',
  female = 'Female'
}

enum HairColor {
  Blond = 'blond'
  // TODO: more
}
enum SkinColor {
  Fair = 'fair'
  // TODO: more
}

export interface People {
  'birth_year': string
  'eye_color': string
  'films': string[]
  'gender': Gender
  'hair_color': HairColor
  'height': string
  'homeworld': string
  'mass': string
  'name': string
  'skin_color': SkinColor
  'created': Date
  'edited': Date
  'species': string[]
  'starships': string[]
  'url': string
  'vehicles': string[]
}

export interface Planet {
  'name': string
  'rotation_period': number
  'orbital_period': number
  'diameter': number
  'climate': string
  'gravity': string
  'terrain': string
  'surface_water': number
  'population': number
  'residents': string[]
  'films': string[]
  'created': Date
  'edited': Date
  'url': string
}
