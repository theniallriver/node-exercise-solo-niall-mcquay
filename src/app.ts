// Please take a look at NIALLSREADME.md if you haven't already
import express from 'express';

import getPeople from './handlers/people';
import getPlanets from './handlers/planets';

const app = express();
app.use(express.json());

app.get('/people', getPeople);
app.get('/planets', getPlanets);

export default app;
