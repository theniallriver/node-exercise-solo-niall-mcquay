module.exports = {
  env: {
    es2021: true,
    node: true
  },
  extends: [
    'airbnb-base',
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended'

  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module'
  },
  plugins: [
    '@typescript-eslint'
  ],
  rules: {
    'no-shadow': 'off',
    '@typescript-eslint/no-shadow': 'error',
    'max-len': ['error', {
      code: 140,
      ignoreTrailingComments: true
    }],
    'import/extensions': [
      'error',
      'ignorePackages',
      {
        js: 'never',
        jsx: 'never',
        ts: 'never',
        tsx: 'never'
      }
    ],
    'import/order': ['error', {
      alphabetize: {
        order: 'asc', /* sort in ascending order. Options: ['ignore', 'asc', 'desc'] */
        caseInsensitive: true /* ignore case. Options: [true, false] */
      },
      'newlines-between': 'always'
    }],
    'comma-dangle': ['error', 'never'],
    'no-nested-ternary': 'off',
    'no-use-before-define': 'off',
    // note you must disable the base rule as it can report incorrect errors
    '@typescript-eslint/no-use-before-define': ['error', { enums: false, functions: false }],
    indent: 'off',
    '@typescript-eslint/type-annotation-spacing': 'error',
    '@typescript-eslint/indent': ['error', 2],
    '@typescript-eslint/space-infix-ops': ['error'],
    '@typescript-eslint/member-delimiter-style': ['error', {
      multiline: {
        delimiter: 'none'
      },
      singleline: {
        delimiter: 'comma',
        requireLast: false
      }
    }]
  },
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.js', '.jsx', '.ts', '.tsx']
      }
    }
  }
};
